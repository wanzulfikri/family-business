import firebase from "firebase/app";
import "firebase/database";
const firebaseConfig = {
    apiKey: "AIzaSyD6Hyoe9NrJQRkRVrLiwS4NANPYRQHjLKw",
    authDomain: "family-business-a4943.firebaseapp.com",
    databaseURL: "https://family-business-a4943-default-rtdb.firebaseio.com",
    projectId: "family-business-a4943",
    storageBucket: "family-business-a4943.appspot.com",
    messagingSenderId: "376009504922",
    appId: "1:376009504922:web:8fce45988c8b9f368e4723",
    measurementId: "G-1W4Z0K9YFD",
};
// Initialize Firebase
const db = firebase.initializeApp(firebaseConfig).database();

export default db;