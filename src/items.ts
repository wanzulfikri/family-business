export default
    [
        { name: "MONAKA", price: 10, group: "ice cream" },
        { name: "Personal Box (Baton)", price: 25, group: "ice cream" },
        { name: "Party Box (Baton)", price: 50, group: "ice cream" }];